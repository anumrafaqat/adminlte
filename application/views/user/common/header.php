
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Products view</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets2/images/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo base_url();?>assets2/apple-touch-icon.png">
    
  
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   <!-- <script src="<?php //echo base_url();?>assets/plugins/jQuery/jQuery-2.2.0.min.js"></script>-->
   <script src="<?php echo base_url();?>assets2/js/vendor/jquery-3.2.1.min.js"></script>
    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/bootstrap.min.css">
      <script src="<?php echo base_url();?>assets2/js/bootstrap.min.js"></script>
    <!-- Owl Carousel min css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/owl.theme.default.min.css">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/core.css">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/shortcode/shortcodes.css">
    <!-- Theme main style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/responsive.css">
    <!-- User style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/custom.css">


    <!-- Modernizr JS -->
    <script src="<?php echo base_url()?>assets2/js/vendor/modernizr-3.5.0.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Body main wrapper start -->
    <div class="wrapper">
        <!-- Start Header Style -->
        <header id="htc__header" class="htc__header__area header--one">
            <!-- Start Mainmenu Area -->
            <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
                <div class="container">
                    <div class="row">
                        <div class="menumenu__container clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-5"> 
                                <div class="logo">
                                     <a href="index.html"><img src="<?php echo base_url();?>/assets2/images/interwood.png" alt="logo images"></a>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-8 col-sm-5 col-xs-3">
                                <nav class="main__menu__nav hidden-xs hidden-sm">
                                    <ul class="main__menu">
                                        <li class="drop"><a href="<?php echo base_url();?>products">Home</a></li>
                                      
                                        <li class="drop"><a href="<?= base_url();?>products">
                                        Product</a>
                                            <ul class="dropdown">
											<?php foreach ($drop_down as $menu) {;?>
												<li><a href="<?php echo base_url();?>products/show_prodgrid/<?= $menu['category']?>"><?=$menu['category'];?></a></li>
                                            <?php };?>  
                                            </ul>
                                        </li>
                                       
                                        
                                        <li><a href="<?php echo base_url();?>contactus">contact</a></li>
										
                                    </ul>
                                </nav>

                                <div class="mobile-menu clearfix visible-xs visible-sm">
                                    <nav id="mobile_dropdown">
                                        <ul>
                                            <li><a href="index.html">Home</a></li>
                                            <li><a href="blog.html">Product</a></li>
                                            <li><a href="#">pages</a>
                                                <ul>
                                                   
                                                </ul>
                                            </li>
                                            <li><a href="<?php echo base_url();?>contactus">contact</a></li>
                                        </ul>
                                    </nav>
                                </div>  
                            </div>
                            <div class="col-md-3 col-lg-2 col-sm-4 col-xs-4">
                                <div class="header__right">
                                    <div class="header__search search search__open">
                                        <a href="#"><i class="icon-magnifier icons"></i></a>
                                    </div>
                                    <div class="header__account">
                                        <a href="#"><i class="icon-user icons"></i></a>
                                    </div>
                                    <div class="htc__shopping__cart">
                                        <a class="" href="<?php echo base_url();?>cart"><i class="icon-handbag icons"></i></a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-menu-area"></div>
                </div>
            </div>
            <!-- End Mainmenu Area -->
        </header>