<?php  $this->load->view('Admin/header'); ?>
<?php $this->load->view('Admin/aside'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <h1>
       Insert Product Category
       
      </h1>
      <ol class="breadcrumb">
         
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product Color</a></li>
        <li class="active">Color Table</li>
    
      </ol>
    </section>
       <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-7" style="margin-left: 200px; margin-top: 150px;">
          <!-- general form elements -->
          <div class="box box-primary" style="margin-top: -15%">
            <div class="box-header with-border">
              <h3 class="box-title">Update Category</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="<?php echo base_url();?>admin/prodcategory/insert">
              <div class="box-body">
               
                  <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text" class="form-control" id="name" placeholder="Enter name" name="name"  required>
                </div>


              </div>
              <div class="box-footer">
             <input type="submit" class="btn btn-success" value="Insert Product Category">
              </div>
            </form>
          </div>
          </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  



<?php $this->load->view('Admin/footer'); ?>