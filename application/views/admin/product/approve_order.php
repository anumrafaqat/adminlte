

<?php $this->load->view('admin/header');?>

<?php $this->load->view('admin/aside');?>

<script src="<?php echo base_url(); ?>assets/plugins/swal/swal.all.min.js"></script>

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Orders   &nbsp&nbsp
         
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/prodcolor">Approve order</a></li>
     
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                   <th>order id</th>
                      <th>Date</th>
                       <th>admin status</th>
                  <th>order status</th>
                  <th>payment status</th>
                   <th>order total</th>
                  <th width="200">Actions</th>
               
                </tr>
                </thead>


                <tbody>
               

            <?php foreach($orders as $row) {; ?>
                <tr>
                   <td><?php echo $row['order_id'];?></td>
                    <td><?php echo $row['order_date'];?></td> 
                  <td><?php echo $row['admin_status'];?></td>
                  <td><?php echo $row['order_status'];?></td>
                  <td><?php echo $row['payment_status'];?>
                  </td>
                 
                  <td><?php echo $row['order_total'];?></td>
                  <td> 
<?php if($row['admin_status']=='approved') { ?>

 <a href="<?php echo base_url();?>admin/orders/approve_order/<?php echo $row['order_id'];?>"  class="disabled btn btn-success" ><i class="fa fa-check-circle" data-toggle="tooltip" title="Approve"></i></a>
<?php } else {  ?>
        	 <a href="<?php echo base_url();?>admin/orders/approve_order/<?php echo $row['order_id'];?>"  class="edit btn btn-success" data-toggle="modal"><i class="fa fa-check-circle" data-toggle="tooltip" title="Approve"></i></a>
<?php } ?>
                     <a href="<?php echo base_url();?>admin/orders/view_order/<?php echo $row['order_id'];?>" class="edit btn btn-primary" data-toggle="modal" title="view"><span>View</span></a>

                  </td>
                </tr>
               
           <?php }; ?>
              
                </tbody>
              </table>
            </div>
    </div> 
    </section>
  </div>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>


            <?php $this->load->view('admin/aside');?>
            <?php $this->load->view('admin/footer');?>