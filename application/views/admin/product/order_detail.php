

<?php $this->load->view('admin/header');?>

<?php $this->load->view('admin/aside');?>

<script src="<?php echo base_url(); ?>assets/plugins/swal/swal.all.min.js"></script>

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Orders Details  &nbsp&nbsp
         
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/prodcolor">Approve order</a></li>
     
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Product</th>
                  <th>Product color</th>
                  <th>size</th>
                  <th>Product price</th>
                   <th>quantity</th>
                  <th>shipping address</th>
                  <th>Biling address</th>
                  <th>order total</th>
                  <th>phone no</th>
                </tr>
                </thead>


                <tbody>
               

           <?php foreach($products as $row) {; ?>
                <tr>
                   <td><?php echo $row['prodname'];?></td>
                  <td><?php echo $row['prodcolor'];?></td>
                  <td><?php echo $row['prodsize'];?>
                  </td>
                  <td><?php echo $row['prodprice'];?></td>
                  <td><?php echo $row['qty'];?></td>
                    <td><?php echo $row['shippingaddress'];?></td>
                      <td><?php echo $row['billingaddress'];?></td>
                        <td><?php echo $row['ordertotal'];?></td>
                          <td><?php echo $row['phone'];?></td>
              
                </tr>
               
           <?php }; ?>
              
                </tbody>
              </table>
            </div>
    </div> 
    </section>
  </div>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>


            <?php $this->load->view('admin/aside');?>
            <?php $this->load->view('admin/footer');?>