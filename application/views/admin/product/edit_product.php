<?php  $this->load->view('Admin/header'); ?>
<?php $this->load->view('Admin/aside'); ?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/select2/select2.min.css">

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <div class="content-wrapper">

    <section class="content-header">
      <h1>
   UpdateProduct
      
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10" style="margin-left: 100px; margin-top: 150px;">
          <!-- general form elements -->
          <div class="box box-primary" style="margin-top: -15%">
            <div class="box-header with-border">
              <h3 class="box-title">Edit/Update product</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" enctype="multipart/form-data" action="<?=base_url();?>admin/product/edit/<?php echo $product['id'];?>">
              <div class="box-body">


                <div class="form-group col-md-6">
                  <label for="name">Product</label>
                  <input type="text" class="form-control" id="name" placeholder="Enter product name" name="name" value="<?= $product['name']; ?>" required>
               </div>

                <div class="form-group col-md-6">
                  <label for="Description">Description</label>
                  <input type="text" name="product_description" class="form-control" id="Description" placeholder="Enter product Description" value="<?= $product['product_description']; ?>"  required>
               </div>

<div class="clearfix"></div>

     <?php $existingCategories = explode(",", $product['category']); ?>
            <div class="form-group col-md-6">
                <label>Category</label>
                <select class="form-control select2 " name="category[]" multiple="multiple"  data-placeholder="Choose Category" tabindex="-1" style="width: 100%;">
                <?php foreach ($category as $data) {;?>
                 <option value="<?= $data['name']; ?>" <?= (in_array($data['name'], $existingCategories))? "selected": "";?> > <?= $data['name'];?></option>
               <?php };?>
                </select>
            </div>


   
    <?php $existingCategories = explode(",", $product['color']); ?>
 <div class="form-group col-md-6">
                <label>Colors</label>
                <select class="form-control select2" name="color[]" multiple  data-placeholder="Choose Colors" tabindex="-1" style="width: 100%;"> 
                <?php foreach ($color as $data) {;?>
                <option value="<?= $data['name']; ?>" <?= (in_array($data['name'], $existingCategories))? "selected": "";?> > <?= $data['name'];?></option>
               <?php };?>
               </select>
</div>

<div class="clearfix"></div>

 <?php $existingCategories = explode(",", $product['size']); ?>
 <div class="form-group col-md-6">
                <label>Sizes</label>
                <select class="form-control select2" name="size[]" multiple  data-placeholder="Choose Sizes" tabindex="-1" style="width: 100%;">
                 <?php foreach ($size as $data) {;?>
                <option value="<?= $data['name']; ?>" <?= (in_array($data['name'], $existingCategories))? "selected": "";?> > <?= $data['name'];?></option>
              <?php  } ;?>
               </select>
 </div>

   
            <div class="form-group col-md-6">
               <label for="price">Add image:</label>
               <input type="file" class="form-control " name="userfile" >
               <label><?php echo $product['userfile'] ?></label>
            </div>

            <div class="clearfix"></div>

             <div class="form-group col-md-6">
                <label for="price">Add other images:</label>
                <input type="file" class="form-control " name="userfile2[]"  multiple />
                <label><?php echo $product['userfile2'] ?></label>
             </div> 

              <div class="form-group col-md-6">
                <label for="price">Price</label>
                <input type="text" class="form-control" id="price" placeholder="Enter price" value="<?= $product['price']; ?>" name="price" >
              </div>
            <div class="clearfix"></div>

                <div class="form-group col-md-6">
                <label for="sale_price">Sale price</label>
                <input type="text" class="form-control" id="sale_price" placeholder="Enter sale price" value="<?= $product['sale_price']; ?>"  name="sale_price" >
                </div>


                  <div class="form-group col-md-6">
                  <label for="stock">Stock</label>
                  <input type="number" value="<?= $product['stock'] ?>" class="form-control" id="stock" placeholder="Enter stock" name="stock" >
                </div>
  
              <div class="box-footer col-md-6">
                <button type="submit" class="btn btn-primary col-md-4">Submit</button>
              </div>

            </form>
          </div>
      </div>
    </section>
  </div>


 <script src="<?php echo base_url();?>assets/plugins/select2/select2.full.min.js"></script>
  <script>
  $(document).ready(function() {
       $(".select2").select2();
  }); 
  </script>




<?php $this->load->view('Admin/footer'); ?>