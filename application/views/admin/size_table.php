

<?php $this->load->view('admin/header');?>

<?php $this->load->view('admin/aside');?>


<script src="<?php echo base_url(); ?>assets/plugins/swal/swal.all.min.js"></script>


 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Product Sizes
        <a href="<?php echo base_url();?>/admin/prodsize/insert" class="btn btn-success">Add New Size</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/prodsize">Product Size Table</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                 
                  <th>Name</th>
                  <th>Slug(s)</th>
                  <th width="100">Actions</th>
               
                </tr>
                </thead>


                <tbody>

                	<?php foreach($query as $row) { ?>
                <tr>
                 
                  <td><?php echo $row->name;?>
                  </td>
                  <td><?php echo $row->slug;?></td>
                  <td> 
                  	 <a href="<?php echo base_url();?>admin/prodsize/get_data/<?php echo $row->id;?>" class="edit btn btn-primary" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                            <a href="<?php echo base_url();?>admin/prodsize/delete/<?php echo $row->id;?>" class="delete btn btn-danger"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>

                   </td>
                
                </tr>
               
           <?php } ?>
              
               
              
                </tbody>
          
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
  </div>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
  $("#example2").dataTable();
}); // document.ready
</script>

<script>
$(document).on("click", ".delete", function(e) {
        var tag = this;
        e.preventDefault();
        swal.fire({
          title: 'Are you sure you want to delete this product size?',
          text: "This action cannot be undone and all the related data will be deleted",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
          if (result.value) {
            window.location.href = $(tag).attr("href");
          } else {
            return false;
          }
        });
      });
  </script>

<?php $this->load->view('admin/footer');?>