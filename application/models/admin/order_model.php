<?php 

class order_model extends CI_Model{

function _construct(){
parent::_contruct();
}
public function display_orders()
{
    $this->db->select();
	$q=$this->db->get('orders')->result_array();
	return $q;
}

public function product_join($id)
{

	$this->db->select('product.name as prodname,order_detail.color as prodcolor,order_detail.size as prodsize,product.price as prodprice, order_detail.quantity as qty,shipping_detail.shipping_address as shippingaddress,shipping_detail.billing_address as billingaddress,orders.order_total as ordertotal,shipping_detail.customer_phone as phone');
	$this->db->join('order_detail', 'product.id = order_detail.product_id' );
	$this->db->join('orders', 'orders.order_id = order_detail.order_id' );
	$this->db->join('shipping_detail', 'orders.customer_id = shipping_detail.cust_id' );
	$query=$this->db->get_where('product',array('orders.order_id'=>$id));
	//$query = $this->db->get('product');
	//print_r($query->result_array());exit;
	//echo $this->db->last_query();exit;
	return $query->result_array();	
}

public function approve($id)
{
$this->db->set('admin_status','approved');
$this->db->where('order_id', $id);
$q=$this->db->update('orders');
return $q;
//echo $this->db->last_query();exit;
}

}