<?php
class Profile extends CI_Controller {

	public function index() {
		//$this->home_page();
		if($this->session->has_userdata('id')) {
            $this->load->model('admin/Profile_model');
			$session_id = $this->session->userdata('id');
			$query2= $this->Profile_model->get_data($session_id);
			//print_r($query2);exit;
			$this->load->view('admin/profile',$query2);
			
		}else{
			redirect('admin/login');
		}
	}



	public function edit()
	{

      $this->load->model('admin/Profile_model');

			$session_id = $this->session->userdata('id');
			if($this->input->post()) {

				$postedData = $this->security->xss_clean($this->input->post());
	            $query = $this->Profile_model->search($postedData,$session_id); 
	          


           
            if(!empty($query)) 
           {

	            if($postedData['email'] != $query['email']) {
					$query1 = $this->Profile_model->match_email($postedData);
					
					if(!empty($query1)){
					   $this->session->set_flashdata('failure','Account with this name already exist');
					   //$this->load->view('admin/profile');
					   redirect('admin/profile');
					}

				}

				else { 
					
					if($postedData['password'] != "") {
						//echo "dsfs";
						$postedData['password'] = password_hash($postedData['password'], PASSWORD_BCRYPT);
						//echo $postedData['password'];
						//exit;
					}
					else {
							//echo "123";
						//echo $postedData['password'];exit;
						$postedData['password'] = $this->session->userdata('password');
					}
						//print_r($postedData['password']);exit;
					$query = $this->Profile_model->update($postedData,$session_id); 
					$this->session->set_flashdata('success','Account successfully updated');
					//$this->load->view('admin/profile');
					  redirect('admin/profile');
				}

				} 

				else

				{
					$query = $this->Profile_model->update($postedData,$session_id); 
					$this->session->set_flashdata('success','Account successfully updated');
					//$this->load->view('admin/profile');
					  redirect('admin/profile');

				}

            }

            else
            {
            	echo " query is empty";
            }

			                   } //end of edit function
       }


