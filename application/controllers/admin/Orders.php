<?php
class orders extends CI_Controller {

		public function index()
		{

				//$this->load->view('admin/Login');
			if($this->session->has_userdata('id')) 
			{
				 $this->load->model('admin/order_model');
				 $userdata['orders']=$this->order_model->display_orders();
				
				//echo '<pre>'; print_r($userdata['products']);exit;
                 $this->load->view('admin/product/approve_order',$userdata);
             }
             else
             {
             	redirect('admin/login');
             }
		}

		public function view_order($id)
		{
			$this->load->model('admin/order_model');
        $productdata['products']=$this->order_model->product_join($id);
         $this->load->view('admin/product/order_detail',$productdata);
		}

		public function approve_order($id)
		{
	   $this->load->model('admin/order_model');
        $productdata['products']=$this->order_model->approve($id);
        //print_r($productdata);exit;
        $productdata['orders']=$this->order_model->display_orders();
        $this->session->set_flashdata('success','order approved');

         $this->load->view('admin/product/approve_order',$productdata);
        //redirect('admin/product/approve_order');
		}
	}