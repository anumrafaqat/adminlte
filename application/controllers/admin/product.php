<?php
class product extends CI_Controller {

public function index() {
	$this->load->library('upload');
	if($this->session->has_userdata('id'))
	{
	$this->load->model('admin/product_model');
    $data['query'] = $this->product_model->search();
    $this->load->view('admin/product/product_table',$data);
}
	else
	{
		redirect('admin/login');
	}

}

public function insert_product()
{
if($this->session->has_userdata('id')) {
	
	$this->load->model('admin/product_model');
	$userdata['categories'] = $this->product_model->getcategory_name();
	$userdata['colors'] = $this->product_model->getcolor_name();
	$userdata['sizes'] = $this->product_model->getsize_name();
	$this->load->view('admin/product/add_product',$userdata);
			
}
else{
	redirect('admin/login');
    }
}


//insert_postdata plus image
////$Dataposted['userfile2'] = implode(',',$Dataposted['userfile2']);
public function addProductProcess(){

$this->load->model('admin/product_model');
	if($this->input->post()) {
		
		$Dataposted = $this->security->xss_clean($this->input->post());
		$Dataposted['category'] = implode(',',$Dataposted['category']);
		$Dataposted['color'] = implode(',',$Dataposted['color']);
		$Dataposted['size']= implode(',',$Dataposted['size']);

	

		$table = 'product';
		$Dataposted['slug'] = slug_generator($Dataposted['name'], $table);
		//print_r($Dataposted);exit;
		$this->product_model->insert_data($Dataposted); 
		$this->session->set_flashdata('success','product successfully inserted');
		if ($this->session->flashdata('success'))
		{
		echo "sdfds";

		$upload1 = $this->upload_files_single($_FILES["userfile"]);	
		$Dataposted['userfile'] = $upload1['upload_data']['file_name'];
		//print_r($upload1);
		
		$upload2 = $this->upload_files_multiple($_FILES["userfile2"]);
		//echo "<pre>" ;print_r($upload2);exit(); 
		$Dataposted['userfile2'] = implode(',',$upload2);
       //print_r($Dataposted);exit();
		$insert_id = $this->db->insert_id();
		$this->product_model->update($Dataposted,$insert_id); 
		redirect('admin/product');
		//pr($upload);exit;
		}
		else
		{
		$this->session->set_flashdata('failure','Not updated successfully');
		redirect('admin/product/edit_product');
		}
		
	}

}

public function edit($id)
{
	
	$this->load->model('admin/product_model');
	if($this->input->post()) {
		
		$Dataposted = $this->security->xss_clean($this->input->post());
		$Dataposted['category'] = implode(',',$Dataposted['category']);
		$Dataposted['color'] = implode(',',$Dataposted['color']);
		$Dataposted['size']= implode(',',$Dataposted['size']);

	

		$table = 'product';
		$Dataposted['slug'] = slug_generator($Dataposted['name'], $table);
		//print_r($Dataposted);exit;
		$this->product_model->update($Dataposted,$id); 
		$this->session->set_flashdata('success','product successfully inserted');

		if ($this->session->flashdata('success'))
		{
		echo "sdfds";

		$upload1 = $this->upload_files_single($_FILES["userfile"]);	
		$Dataposted['userfile'] = $upload1['upload_data']['file_name'];
		//print_r($upload1);
		
		$upload2 = $this->upload_files_multiple($_FILES["userfile2"]);
		//echo "<pre>" ;print_r($upload2);exit(); 
		$Dataposted['userfile2'] = implode(',',$upload2);
       //print_r($Dataposted);exit();
		//$insert_id = $this->db->insert_id();
		$this->product_model->update($Dataposted,$id); 
		redirect('admin/product');
		//pr($upload);exit;
		}
		else
		{
		$this->session->set_flashdata('failure','Not updated successfully');
		redirect('admin/product/edit_product');
		}
		
	}
}


//Edit button when clicked on product table

	public function edit_product($id)
{
	//echo $id;
	$this->load->model('admin/product_model');
	if($id != "") {
	$userdata['product']=  $this->product_model->modaldata($id);
		if(empty($userdata['product'])) {
		$this->session->set_flashdata('failure','No product found !');
			redirect('admin/product/product_table');
		}
	}
	$userdata['category'] = $this->product_model->getcategory_name();
	$userdata['color'] = $this->product_model->getcolor_name();
	$userdata['size'] = $this->product_model->getsize_name();
	{$this->load->view('admin/product/edit_product',$userdata);}

   }


public function delete($id)
{
if($this->session->has_userdata('id')) {

	$this->load->model('admin/product_model');
	if($id != "") {
		$result = $this->product_model->deletebyid($id);
		$this->session->set_flashdata('success','Delete successfully');
        redirect('admin/product');
	}
	else
	{
		$this->session->set_flashdata('failure','Doesnot deleted successfully, select records to delete');
		redirect('admin/product');
    }
}
	else
	{
		redirect('admin/login');
	}
}  //end of delete

public function upload_files_single($fieldname) {
	$data = NULL;
	$_FILES['c_img']['name'] = $fieldname['name'];
    $_FILES['c_img']['type'] = $fieldname['type'];
    $_FILES['c_img']['tmp_name'] = $fieldname['tmp_name'];
    $_FILES['c_img']['error'] = $fieldname['error'];
    $_FILES['c_img']['size'] = $fieldname['size'];

	$config['upload_path'] = 'uploads/products';
	$config['allowed_types']        = 'gif|jpg|png';
    //$config['max_size']             = 1000;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;
	
	$this->load->library('upload', $config);
	$this->upload->initialize($config);
	$data['upload_data'] = '';
	if (!$this->upload->do_upload('c_img')) {
		$data = array('msg' => $this->upload->display_errors());
	} else {
		$data = array('msg' => "success");
		$data['upload_data'] = $this->upload->data();
	}
	return $data;
}

public function upload_files_multiple($fieldname) {
	$data = NULL;
	echo "sdsda";
	 $filesCount = count($_FILES['userfile2']['name']);

            for($i = 0; $i < $filesCount; $i++){
    $_FILES['file']['name']     = $_FILES['userfile2']['name'][$i];
    $_FILES['file']['type']     = $_FILES['userfile2']['type'][$i];
    $_FILES['file']['tmp_name'] = $_FILES['userfile2']['tmp_name'][$i];
    $_FILES['file']['error']     = $_FILES['userfile2']['error'][$i];
   // $_FILES['file']['size']     = $_FILES['userfile2']['size'][$i];

	$config['upload_path'] = 'uploads/products';
	$config['allowed_types']        = 'gif|jpg|png';

	
	$this->load->library('upload', $config);
	$this->upload->initialize($config);
	//$data['upload_data'] = '';
	if (!$this->upload->do_upload('file')) {
		$data = array('msg' => $this->upload->display_errors());
		//echo "here";exit;
	} 
	 else {
		$data = array('msg' => "success");
		$data['upload_data'] = $this->upload->data();
		$count[$i]=$data['upload_data']['file_name'];
	      }
} //end of for
//print_r($count);exit();
return $count;
}
}