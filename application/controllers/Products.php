<?php
class products extends CI_Controller {

		public function index()
		{

				//$this->load->view('admin/Login');
				 $this->load->model('user/E_store_product');
				 $userdata['drop_down'] = $this->E_store_product->dropdown_data();
				 $userdata['products'] = $this->E_store_product->get_products();
				 //print_r($userdata['products']);exit;
                 $this->load->view('user/product_page',$userdata);
		}

		public function show_prodgrid($category)
		{
	    $this->load->model('user/E_store_product');
	     $userdata['drop_down'] = $this->E_store_product->dropdown_data();
	     $userdata['product_grid'] = $this->E_store_product->product_data($category);
	     $userdata['product_category'] = $this->E_store_product->getcategory_data();
	     $userdata['product_size'] = $this->E_store_product->getsize_data();
		 $this->load->view('user/product_grid',$userdata);
		}


		public function filtered_products()
		{
	 $this->load->model('user/E_store_product');
	  if($this->input->post()) {
	 	//echo "1";
     $filter = $this->security->xss_clean($this->input->post('search'));
     $str_arr = explode ("-", $filter);  
    
     $userdata['drop_down'] = $this->E_store_product->dropdown_data();
	$userdata['product_category'] = $this->E_store_product->getcategory_data();
	$userdata['product_size'] = $this->E_store_product->getsize_data();
  //print_r($str_arr);exit;
     $userdata['product_grid']=$this->E_store_product->filtered_products($str_arr);
    

	 $this->load->view('user/product_grid',$userdata);
		}
		}


			public function show_proddetail($id)
		{
	    $this->load->model('user/E_store_product');
	     $userdata['drop_down'] = $this->E_store_product->dropdown_data();
	     $userdata['product_detail'] = $this->E_store_product->product_details($id);
		 $this->load->view('user/product_details',$userdata);
		}


		public function search()
		{
		 $this->load->model('user/E_store_product');
		  if($this->input->post()) {

		     $user = $this->security->xss_clean($this->input->post('search'));
				$userdata['drop_down'] = $this->E_store_product->dropdown_data();
				$userdata['product_category'] = $this->E_store_product->getcategory_data();
				$userdata['product_size'] = $this->E_store_product->getsize_data();
		     //echo $userdata;exit;
			    $userdata['product_grid']=$this->E_store_product->search_data($user);
			    //print_r($userdata);exit;
				$this->load->view('user/product_grid',$userdata);
			}
		}


}	
			

