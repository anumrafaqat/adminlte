<?php
class cart extends CI_Controller {
    
		public function index()
		{$this->load->library('cart');
				$this->load->model('user/E_store_product');
				 $userdata['drop_down'] = $this->E_store_product->dropdown_data();
                 $this->load->view('user/cart_page',$userdata);
		}

		public function add_cart($id)
		{
			$this->load->model('user/E_store_product');
			 $this->load->model('user/cart_model');
		if($this->input->post()) {
  		$userdata['postedData'] = $this->security->xss_clean($this->input->post());
  		$userdata['cart_data']=$this->cart_model->cart_data($id);
  		   $userdata['drop_down'] = $this->E_store_product->dropdown_data();
  		$data = array(
        'id'      => $userdata['cart_data']['id'],
        'qty'     => $userdata['postedData']['quantity'],
        'price'   => $userdata['cart_data']['sale_price'],
        'name'    => $userdata['cart_data']['name'],
        'image'   => $userdata['cart_data']['userfile'],
        'sale_price'   => $userdata['cart_data']['price'],
        'options' => array('Size' => $userdata['postedData']['size'], 'Color' => $userdata['postedData']['color'])
);

// echo "<pre>";print_r($data);exit;
  		 $this->cart->insert($data);
  		// echo "<pre>";print_r($this->cart->contents());exit;
        $this->load->view('user/cart_page',$userdata);

		}
}


		public function delete_cartitem($rowid)	{
//$cartcontents = $this->cart->product_contents['rowid'];
$deleteItem = array(
    'rowid' => $rowid,
    'qty'   => 0
);
$this->cart->update($deleteItem);
$this->session->set_flashdata('success','successfully deleted');
redirect('cart');
		}

    public function update_cart() {
//echo "<pre>";print_r($this->cart->contents());
            if($this->input->post()) {
$userdata = $this->security->xss_clean($this->input->post());
//echo '<pre>'; print_r($userdata);exit;
$rows = $userdata['rowid'];
$qtys = $userdata['qty'];

foreach($qtys as $key => $item){
$updateItem = array(
    'rowid' =>  $rows[$key],
    'qty'   => $qtys[$key]
);
//echo '<pre>'; print_r($updateItem);//exit;
$this->cart->update($updateItem);
}
//echo "<pre>";print_r($this->cart->contents());exit;
redirect('cart');
    }

}
    public function order_info($total)
    {
    
    $this->load->model('user/cart_model');
    $userdata=$this->cart->contents();
   // $this->session->set_flashdata('success','Successfully inserted');
    $d = date("Y-m-d") ;
//echo $d;exit;
  $data=array(
   'order_id' => '' ,
   'order_status' => 'pending',
   'payment_status' => 'pending',
    'order_date' => $d,
    'customer_id'=>'',
    'order_total'=> $total,
     'order_GST' => '16',
     'shipping_amount' => '1000',
     'admin_status'=>'disapproved'
    );
   $this->cart_model->insert_order($data);
  $orderid =$this->db->insert_id();
  $this->session->set_userdata('orderid',$orderid);

//echo "<pre>";print_r($userdata);exit;
foreach($userdata as $indice => $cartprod){
  $orderdetails = array(
'orderdetail_id' => '',
'order_id' => $orderid,
'product_id' => $cartprod['id'],
'price' => $cartprod['price'],
'quantity' => $cartprod['qty'],
'Color' => $cartprod['options']['Color'],
'Size' => $cartprod['options']['Size']
);
    $this->cart_model->insert_orderdetails($orderdetails);
}

  //echo "<pre>";print_r($userdata);exit;
   // print_r($data);exit();
//$this->session->set_flashdata('success','order placed successfully');
    redirect('checkout');



    }
}
